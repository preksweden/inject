/*
 * Custom code, dont use this as an url
 * Name: Fill in forms, honeypot
 * Auto Run: "off"
*/
const values =
  {
    firstname: 'PREK',
    lastname: 'MAIL',
    mail: 'andreas.lundgren@prek.se',
    cellphone: '0700000000',
  };
Object.keys(values).forEach(function(element) {
  const matchedElements = document.querySelectorAll('input[name="' + element + '"]');
  if (matchedElements.length) {
    matchedElements.forEach(function(matchedElement) {
      matchedElement.setAttribute('value', values[element]);
    });
  }
});
const honeypots = document.querySelectorAll('input.ohnohoney');
if (honeypots.length) {
  honeypots.forEach(function(honeypot){
    honeypot.setAttribute('value', 'prek-test-honeypot');
  });
}
