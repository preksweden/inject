/*
 * Name: PHP-versions with colors
 * Auto Run: "url"
 * Auto Run Url: "https://prek.myjetbrains.com/youtrack/articles/WEBB-A-19/PHP-versioner"
*/
jQuery(document).ready(function($){
    setTimeout(function () {
        $('.c_tableWrapper__a48 td').each(function(i,e){
            if($(e).text().indexOf('Extern hosting') >= 0){
                $(e).css({'background-color': '#e6e6e6', 'color': '#424242'});
            } else if($(e).text().indexOf('7.2') >= 0){
                $(e).css({'background-color': '#e64c4c', 'color': '#424242'});
            } else if($(e).text().indexOf('7.4') >= 0){
                //$(e).css({'background-color': '#4c99e6', 'color': '#424242'});
            } else if($(e).text().indexOf('8.0') >= 0){
                $(e).css({'background-color': '#e6994c', 'color': '#424242'});
            } else if($(e).text().indexOf('8.2') >= 0){
                $(e).css({'background-color': '#4ce699', 'color': '#424242'});
            }
        })
    }, 1000);
});
