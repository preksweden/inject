/*
 * Name: Show all listings with showing
 * Auto Run: "off"
 */
var showingListings = document.querySelectorAll("[data-cy-showing=\"1\"]");
let foundShowingListings = 0;
if (showingListings.length === 0) {
  showingListings = document.querySelectorAll("[data-cy=\"showing\"]");
}
if (showingListings.length) {
  showingListings.forEach(showingObject => {
    if (showingObject.nodeName === "A") {
      showingObject = showingObject.parentNode;
    }
    if (showingObject) {
      showingObject.style.border = "5px dashed red";
      foundShowingListings++;
    }
  });
}
console.log("found " + foundShowingListings + " objects");
