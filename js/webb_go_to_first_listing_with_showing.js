/*
 * Name: Go to first listing with showing
 * Auto Run: "off"
 */
var showingListing = document.querySelectorAll("[data-cy-showing=\"1\"]");
if (showingListing.length === 0) {
  showingListing = document.querySelectorAll("[data-cy=\"showing\"]");
}
if (showingListing.length) {
  if (showingListing[0].nodeName !== "A") {
    showingListing = showingListing[0].querySelectorAll("a");
  }
  if (showingListing.length) {
    window.location.href = showingListing[0].getAttribute("href");
  }
}
