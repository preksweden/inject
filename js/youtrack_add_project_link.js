/*
 * Name: YT project
 * Auto Run: "url"
 * Auto Run Url: "https://prek.myjetbrains.com/youtrack/*"
*/

(function() {
  let polling = true;
  (function checkProjectLink() {
    if (polling) {
      var pathArray = window.location.pathname.split("/");
      if (pathArray.length) {
        var issue = "";
        for (i = 0; i < pathArray.length; i++) {
          if (pathArray[i] === "issue") {
            var key = i + 1;
            issue = pathArray[key];
          }
        }
        if (issue) {
          var match = issue.match(/(^[A-Z]+)/)[0];
          if (match) {
            var title = "To project";
            var span = document.createElement("span");
            span.setAttribute("id", "toProjectLink");
            span.classList.add("ring-header__menu-item");
            var a = document.createElement("a");
            var linkText = document.createTextNode(title);
            a.appendChild(linkText);
            a.title = title;
            a.href = "https://prek.myjetbrains.com/youtrack/issues/" + match;
            a.classList.add("ring-link");
            span.appendChild(a);
            var menu = document.getElementById("headerNavigationMenuItems");
            if (menu) {
              menu.prepend(span);
              polling = false;
            }
          }
        } else {
          var projectLink = document.getElementById("toProjectLink");
          if (projectLink.length) {
            polling = false;
            projectLink.remove();
          }
        }
      }
      setTimeout(function() {
        checkProjectLink();
      }, 2000);
    }
  }());
}());
