/*
 * Custom code, dont use this as an url
 * Name: Fill in forms
 * Auto Run: "off"
*/
const values =
  {
    firstname: "PREK",
    lastname: "MAIL",
    mail: "dennis@prek.se",
    cellphone: "0700000000",
    address: "S:t Larsgatan 3",
    message: "Testmeddelande",
    acceptance: true
  };
Object.keys(values).forEach(function (key) {
  const matchedElements = document.querySelectorAll(`[name="${key}"]`);
  matchedElements.forEach(function (matchedElement) {
    if (matchedElement.type === "checkbox") {
      matchedElement.checked = values[key];
      matchedElement.dispatchEvent(new Event("change", { bubbles: true }));
    } else {
      matchedElement.value = values[key];
    }
  })
});
