/*
 * Name: YouTrack Sprint Link
 * Auto Run: "url"
 * Auto Run Url: "https://prek.myjetbrains.com/youtrack/*"
*/
(function() {

  let polling = true;
  (function checkSprintLink() {
    if (polling) {
      const existingElement = document.querySelector('[data-test="ring-link agile-boards-button"]');
      if (existingElement) {
        const newElement = existingElement.cloneNode(true);
        newElement.href = "/youtrack/agiles/63-17/current?query=for:%20me%20or%20(Reviewer:%20me%20and%20State:%20%7BReady%20for%20CR%7D)%20or%20(Assigned%20tester:%20me%20and%20(State:%20%7BTesting,%20deployed%7D%20or%20tag:%20%7BTest%20on%20dev%7D))"; 
        newElement.setAttribute("data-test", "ring-link sprint-button");
        newElement.classList.remove("c_active__dbf");
        const tooltipSpan = newElement.querySelector('[data-test="ring-tooltip agile-boards"]');
        if (tooltipSpan) {
          tooltipSpan.setAttribute("data-test-title", "My sprint");
        }
        const textSpan = newElement.querySelector('[data-test="agile-boards-title"]');
        if (textSpan) {
         textSpan.setAttribute("data-test", "my-sprint-title");
         textSpan.textContent = "My sprint";
        }
        existingElement.parentNode.appendChild(newElement);
        polling = false;
      }

      setTimeout(function() {
        checkSprintLink();
      }, 2000);
    }
  }());
}());

