/*
 * Name: "Set demo deploy repo"
 * Auto Run: "url" 
 * Auto Run Url: "http://jenkins02.prek.srv:8080/view/Customer%20Demo/job/DEMO_WP_Dynamic_Bedrock_php7_node_config/*"
 * Inject page: https://preksweden.gitlab.io/inject/js/jenkins_set_demo_deploy_repo.js
*/

const gitlabRepo = new URLSearchParams(window.location.search).get("gitlabRepo");
const gitlabBranch = new URLSearchParams(window.location.search).get("gitlabBranch");
if (gitlabRepo) {
  const input = document.querySelector("[value=\"gitRepo\"]");
  if (input) {
    const select = input.nextSibling;
    if (select) {
      select.value = "git@gitlab.prek.srv:wordpress-sites/" + gitlabRepo + ".git";
    }
  }
}
if (gitlabBranch) {
  const input = document.querySelector("[value=\"staging\"]");
  if (input) {
      input.value = gitlabBranch;
  }
}
