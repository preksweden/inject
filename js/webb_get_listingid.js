const listingMeta = document.querySelectorAll("meta[name=listing]");
if (listingMeta.length > 0) {
  const CopyContent = async (listingId) => {
    try {
      await navigator.clipboard.writeText(listingId);
      console.log("copied to clipboard");
    } catch (err) {
      alert(listingId);
    }
  };
  listingMeta.forEach(function(element) {
    const listingId = element.getAttribute("content");
    if (listingId) {
      CopyContent(listingId);
    }
  });
}else{
  alert("couldn't fetch listing id");
}
