(function() {
  let polling = true;
  (function addListenerToVaultSearch() {
    if (polling) {
      let itemFilter = document.querySelector(".vault-filters .filter-button[title=\"Vault: All vaults\"]");
      let orgFilter = document.querySelector(".vault-filters .filter-button[title=\"Filter: All items\"]");
      let search = document.querySelector(".vault-filters input[type=\"search\"]");
      console.log(search);
      console.log(itemFilter);
      console.log(orgFilter);
      if (search && itemFilter && orgFilter) {
        search.addEventListener("keyup", (event) => {
          itemFilter.click();
          orgFilter.click();
          let value = search.value;
          if (!value.endsWith("*")) {
            //append * to searchfield
            value = value + "*";
            search.value = value;
            search.setSelectionRange(search.value.length - 1, search.value.length - 1);
          } else {
            if (value.length === 1) {
              search.value = "";
            }
          }
        });
        polling = false;
      }
      setTimeout(function() {
        addListenerToVaultSearch();
      }, 2000);

    }
  }());
}());