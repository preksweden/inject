const ul = document.querySelectorAll(".merge-request-tabs-holder .merge-request-tabs");
const branches = Array.from(document.querySelectorAll(".detail-page-description .ref-container"));
const jenkinJobs = {
  'fasad.eu': 'FasadEU',
  'harald-haglund': 'harald',
  'infoflex': 'Infoflex_WWW',
  'infoflex-kund': 'Infoflex_Kund',
  'karlsson-uddare-2021': 'karlsson_uddare',
  'levin': 'levinmakleri',
  'perjansson': 'per_jansson',
  'sjomanfrisk': 'SjömanPartners',
  'standardsite': 'fasadweb',
  'tcs': 'tailings',
  'tk-traktordelar': 'tktraktordelar',
  'urbanbyesny': 'urban'
};
const jenkinJobsOnlyLink = [
   'behrer',
   'bjurfors',
   'bocentrum',
   'gadelius',
   'infoflex',
   'infoflex-kund',
   'liwing',
   'matsholmgren',
   'nextor',
   'perjansson',
   'rowaco',
   'strandvagens',
   'tcs',
   'tk-traktordelar',
   'vision'
];
const infoflexJobs = [
  'infoflex',
  'infoflex-kund'
];
if (ul.length) {
  const jenkinsBaseUrl = "http://jenkins02.prek.srv:8080/view/";
  let currentRepo = "";
  let prodUrl = jenkinsBaseUrl;
  let demoUrl = jenkinsBaseUrl + "Customer%20Demo/job/";
  let toBranch = "";
  if (branches.length > 0) {
    const lastNode = branches.slice(-1)[0];
    if(lastNode !== undefined) {
      toBranch = lastNode.innerHTML;
    }
  }
  const urlmatch = window.location.href.match("wordpress-sites\/(.*)\/-\/");
  if (urlmatch.length >= 2) {
    currentRepo = urlmatch[1];
  }
  if (currentRepo === "behrer") {
    demoUrl += "Behrer_Demosite_Wordpress_Bedrock_php8.2";
  } else {
    demoUrl += "DEMO_WP_Dynamic_Bedrock_php7_node_config"
  }
  if (infoflexJobs.includes(currentRepo)) {
    prodUrl += "Infoflex";
  } else {
    prodUrl += "Wordpress%20Production";
  }
  prodUrl += "/job/";
  if (jenkinJobs.hasOwnProperty(currentRepo)) {
    prodUrl += jenkinJobs[currentRepo];
  } else {
    prodUrl += currentRepo;
  }
  prodUrl += "_Production_Deploy_CV";
  if (!jenkinJobsOnlyLink.includes(currentRepo)) {
    prodUrl += '/build'
  }
  const links = [
    {
      title: "Build demo",
      url: demoUrl + "/build?delay=0sec&gitlabRepo=" + currentRepo + "&gitlabBranch=" + toBranch,
    },
    {
      title: "Build prod",
      url: prodUrl,
    },
  ];
  links.forEach((item) => {
    var li = document.createElement("li");
    var a = document.createElement("a");
    var linkText = document.createTextNode(item.title);
    a.appendChild(linkText);
    a.title = item.title;
    a.href = item.url;
    li.appendChild(a);
    ul[0].appendChild(li);
  });
}
